// démarre l'enregistrement le plus tôt possible (buffer-isera en attendant Google)
const spawn = require('child_process').spawn;
const api = require('@google-cloud/speech');

function now() {
    return (new Date()).getTime() + ': ';
}

module.exports = function (node, auth, silenceTimeout, device, finalCallback, notify) {
    silenceTimeout = parseInt("" + (silenceTimeout || 3));

    const options = {
        env: {
            PATH: process.env.PATH
        }
    };

    if (device) {
        options.env.AUDIODEV = device;
    }

    // démarre un fils "sox"
    // les paramêtres sox doivent coller avec ceux de la require Google
    // console.log(now() + "start of sox");
    const sox = spawn('sox', ['-d', '-t', 'wav', '-b', '16', '-r', '16k', '-c', ' 1', '-q', '-'], options);
    sox.on('error', function (error) {
        notify && notify({
            fill: "red",
            shape: "dot",
            text: "sox error"
        });
        node.error(error);
    });

    let lastUtterance = null;
    let timeout = null;

    function onError() {
        // coupe l'enregistrement
        // console.log(now() + "killing sox (on error)");
        sox.kill();
        if (lastUtterance) {
            finalCallback(lastUtterance);
        }
        if (notify) {
            notify();
        }
    }

    function onTimeout() {
        // coupe l'enregistrement
        // console.log(now() + "killing sox (on timeout)");
        sox.kill();
        send();
        if (notify) {
            notify();
        }
    }

    function resetTimeout() {
        if (timeout) {
            clearTimeout(timeout);
        }
        timeout = setTimeout(onTimeout, silenceTimeout * 1000);
    }

    function send() {
        if (lastUtterance) {
            lastUtterance = lastUtterance + ".";
            finalCallback(lastUtterance);
            lastUtterance = null;
        }
    }

    function analyze() {
        // console.log(now() + "start of speech analyzer");
        resetTimeout();

        // client Google api Speech
        const client = new api.SpeechClient({
            credentials: auth
        });

        const request = {
            singleUtterance: true,
            interimResults: true,
            config: {
                // paramêtres mis en avant dans les exemples de la doc Google
                encoding: 'LINEAR16', // sox -t flac -b 16 (bits) -c 1 (mono)
                sampleRateHertz: 16000, // sox -r 16k (Hz)
                languageCode: 'fr-FR',
                enableAutomaticPunctuation: true
            },
        };

        const speechApi = client.streamingRecognize(request);

        speechApi.on('error', function (error) {
            // coupe l'enregistrement
            onError();
            console.error(error);
        });

        speechApi.on('data', function (data) {
            if (data.results.length) {
                resetTimeout();
                data.results.forEach(function (result) {
                    result.alternatives.forEach(function (alternative) {
                        lastUtterance = alternative.transcript;
                        if (notify) {
                            notify({text: lastUtterance});
                        }
                    });
                    if (result.isFinal) {
                        send();
                        // redémarre un nouveau client Google speech
                        // car celui en cours ne va plus faire d'analyze (singleUtterance)
                        analyze();
                    }
                });
            }
        });

        // branche le son vers le client
        sox.stdout.pipe(speechApi);
    }

    analyze();

    return sox;
};