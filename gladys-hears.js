const speech = require('./speech');

module.exports = function (RED) {
    "use strict";

    function Hears(config) {
        let node = this;
        RED.nodes.createNode(node, config); // à garder en début de fonction

        function noStatus() {
            node.status({});
        }

        node.googleAuth = RED.nodes.getNode(config.googleAuth);

        // node.log("gladys-hears' config " + JSON.stringify(config));

        let sox;

        node.on('input', function (msg) {
            // node.log("Something happened");
            // node.warn("Something happened you should know about");
            // node.error("Oh no, something bad happened");
            // node.error("Oh no, something bad happened", msg); // trigger node !Catch in flow
            // node.trace("Log some internal detail not needed for normal operation");
            // node.debug("Log something more details for debugging the node's behaviour");
            // node.log("gladys-hears'd received " + JSON.stringify(msg));

            node.status({
                fill: "green",
                shape: "dot",
                text: "hearing"
            });

            sox = speech(node, node.googleAuth.key, config.silenceTimeout, config.device,
                function (text) {
                    node.send({payload: text});
                    setTimeout(noStatus, 200);
                }, function (notification) {
                    if (notification) {
                        notification = Object.assign({
                            fill: "green",
                            shape: "dot",
                        }, notification);
                    } else {
                        notification = {};
                    }
                    // console.log("notification", notification);
                    node.status(notification);
                });

            // node.send(msg);
            // node.send([msg1, msg2]);
            // node.send([msg1, msg2], msg3);
        });

        node.on('close', function (removed, done) {
            if (removed) {
                // This node has been deleted
            } else {
                // This node is being restarted
            }
            if (sox) {
                sox.kill();
            }
            done();
        });

    }

    RED.nodes.registerType("gladys-hears", Hears, {
        settings: {
            // value field specifies the default value the setting should take.
            // exportable tells the runtime to make the setting available to the editor.
            gladysHearsSilenceTimeout: {
                value: 3,
                exportable: true
            },
            gladysHearsDevice: {
                value: null,
                exportable: true
            }
        }
    });
};
